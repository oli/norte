## 26.

### El Bolsón, Rio Negro

O ruído das palmas era súbito mais forte que o murmúrio da multidão. Ritmadas, quase monótonas, provocadas com o propósito claro de chamar a atenção de quem estivesse distraído com as miudezas da feira de artesanato, as empanadas caseiras, as frutas vermelhas, os potes de geleia. Téo, do outro lado da praça, escutou-as como um chamado de outro tempo: os anos vividos no subúrbio de Buenos Aires e esse solidário hábito argentino de convocar a gente a bater palmas quando se encontrava criança perdida num aglomerado de gente. Seu primo Pablo, diziam, vivia se perdendo quando pequeno.

Pensou nos últimos meses, no último ano: a sina de investigações complicadas quando sua especialidade

(o que de fato sabia fazer)

era seguir cônjuge infiel.

Que ele sabia de encontrar gente desaparecida?

Cruzou o cimento até a borda da praça, por onde se estendia a feira de fim de semana. Mais gente se havia juntado ao chamado, e as palmas marcavam seus passos. Percebeu em algumas senhoras o olhar esperançoso de que fosse ele o pai encontrado.

Um menino de seis ou sete anos estava em pé no epicentro do ruído. Uma mulher de uns trinta anos, ao lado dele, parecia decidida a manter o ritmo enquanto não aparecesse um responsável para reclamar a criança. Ela usava calças largas e coloridas, como muitos dos artesãos, e um agasalho de lã de alpaca com a estampa andina de lhamas enfileiradas, as mangas puxadas até o cotovelo. Um lenço marrom lhe cobria a cabeça e deixava escapar um par de tranças de um castanho claro, quase loiro. Seria muito branca não fosse o óbvio bronzeado e algo dessa poeira patagônica que se agarra à pele e se infiltra na alma de tudo e todos. Os pés, descalços, estavam imundos.

Téo se juntou às palmas. Uma moça jovem se aproximou e tomou o menino nos braços. As palmas cessaram, a mãe agradeceu a mulher de tranças e as pessoas seguiram com seus passeios.

A artesã retomou seu posto atrás de uma das mesas de pulseiras e brincos e Téo voltou a pensar em seu primo mais velho: a insistência de Pablo contra aquela busca e a fúria em seu olhar na última vez que se viram, um dia antes que Téo tomasse o rumo de Córdoba.

— _Yo no quiero encontrarlo_ — Pablo havia dito. — _No me interesa que pasó._

Era estar dividido entre o companheirismo futebolístico e a fraternidade intelectual: Pablo, o primo mais velho, companhia para os estádios e para as partidas na televisão, o amor incondicional ao Racing Club; Roberto, o primo artista, companhia nos gostos musicais e filmes obscuros e ouvir jazz na cozinha noite adentro com um mate entre eles.

Por que desaparecem os adultos? As crianças se distraem, se confundem, confiam demais. Não aprenderam ainda a medir o tamanho do mundo e jamais esperam que os pais lhes tirem os olhos de cima. Téo mirou a mulher das calças coloridas e quis imaginar a criança que havia sido. Aquele mundo — o artesanato, as feiras e os turistas, o caminhar descalço sobre o asfalto — havia sido sempre o seu? Como as crianças que brincavam no gramado da praça, sujas, soltas, juntando pedaços de papelão e canudinhos usados; filhos desse universo de roupas coloridas e alpargatas remendadas, mochilas e nomadismo.

Ou haveria um dia olhado em volta a cidade e o trânsito e a pressa de chegar a um novo emprego e percebido que não importava, que nada daquilo tinha sentido e mais lógico seria separar um par de roupas, metê-las numa mochila e tomar o rumo do sul.

Por quê?

Seria talvez cretino se perguntar _por que_, mas Téo não pôde evitar o eco. Era a pista que restava, a única informação que poderia de fato ajudá-lo a encontrar aquele homem de sessenta e oito anos que mais de trinta anos antes havia abandonado a esposa e dois filhos pequenos e desaparecido, levando consigo uma maleta de roupas, o rádio de pilha, dois pares de sapatos e o jornal do dia. Havia deixado sobre a mesa a aliança de casamento, um pacote de pão e um maço de dinheiro atado com um elástico: quase todo o salário que havia recebido no dia anterior.

Fazia quase um mês desde o começo da investigação. Téo deixava-se arrastar pelos dias deslizando em direção ao sul por pura inércia — as certezas todas desfeitas tanto mais concretas lhe chegavam as informações. O tio desaparecido se havia transformado numa ideia improvável: a figura difusa de um pai ausente, um ex-caminhoneiro que não sabe viver longe da estrada, todos os homens que alguma vez abandonaram um casamento por

(por quê?)

Perguntava-se de que lhe havia adiantado: o percurso que havia feito até ali atrás de migalhas de passado em busca de uma coincidência que daria sentido às semanas anteriores. O vento da Patagônia aos pés da cordilheira não permitia que se assentassem as pegadas: desapareciam os rastros e permanecia a paisagem, renovada e soberana.

Sentia-se exausto.

Mais alguns dias, uma semana. Era hora de desistir e voltar para casa.

## 1.

### São Paulo, SP

— Não!

Era mais reação do que negação: o hábito de dizer _não_ a tudo que lhe escapava do controle. Gustavo riu porque conhecia bem o homem sentado do outro lado da mesa; tinham alguns meses juntos, num relacionamento de idas e vindas e recomeços e incertezas. Sabia que Téo dizia _não_ por padrão, antes mesmo que compreendesse a pergunta.

— Por que não? — Gustavo tinha o corpo inclinado adiante e as mãos sobre a mesa, por hábito dosando cauteloso a distância entre eles. — Tenho umas semanas de folga e não conheço a Argentina.

— Não vai ser viagem de férias.

Também Téo não conhecia tanto assim da Argentina, apesar de ter morado em Avellaneda por quase quatro anos, na época do colégio, na casa da tia. Se voltava era sempre a Buenos Aires para visitar a família. Ainda assim parecia inadequado misturar investigação com passeio — a concentração necessária para a busca do tio desaparecido e a distração que seria a presença de Gustavo com uma câmera fotográfica e sorrisos.

— É trabalho — Téo insistiu.

— Eu posso ajudar.

— Ajudar como?

— Você não dirige.

Era também o que mais lhe custava: explicar-se, justificar-se. Dizer que a partir das sete estou em casa e fui ao cinema e não avisei porque sim e porque quis. Gustavo percebia as implicâncias e mudava de estratégia, dava-lhe o espaço que precisava, passava uma semana ocupado e súbito aparecia na agência com seu sotaque carioca desterrado, seu sorriso de advogado e um convite para o almoço. Entre seus silêncios e as ausências calculadas de Gustavo

(podia chamar aquilo relacionamento?)

era como se Téo fugisse da definição por vício de profissão — detetive particular especializado em investigar cônjuges infiéis. Que relacionamento pode durar sem que uma das partes estivesse vivendo algum tipo de vida dupla?

Às vezes sequer sabia se estavam juntos.

— Você nem gosta de dirigir — Téo disse.

— Eu não gosto de dirigir em São Paulo.

Fora da sala, na recepção, a secretária e Elisa conversavam num murmúrio, quase por não incomodar a discussão que imaginavam atrás da porta fechada. Se Téo fosse menos reservado em relação a Gustavo, Elisa não precisaria tanto malabarismo para entender o que se passava na cabeça do amigo. Se nem Téo sabia chamar aquilo _relacionamento_, que ia dizer Elisa.

— Você lembra o que aconteceu a última vez que pedi sua ajuda com trabalho.

Gustavo esticou os braços por sobre a mesa até tocar as mãos de Téo.

— Já sou grandinho. Se digo que quero ir, e depois me arrependo, o problema é meu. Já te disse: não estamos amarrados.

Não havia razão para tanto incômodo. Gustavo só fazia todo tempo demonstrar que o entendia e o aceitava e não estava ali para mudá-lo ou obrigá-lo a andar de mãos dadas na rua. Se também Gustavo não gostava de andar de mãos dadas na rua. Que mais ele precisava? Essa companhia tão paciente e o espaço para se esconder num cinema no fim da tarde sem avisar ninguém.

— Você conheceu minha mãe — Gustavo disse.

Era quase uma acusação. Téo recolheu as mãos para si. Sentia o rosto quente e teve vontade de sumir. Gustavo o encarava insistente, a camisa desabotoada no pescoço e a gravata afrouxada. Depois endireitou-se na cadeira, olhou a desordem sobre a mesa com ímpetos talvez de empilhar num canto os papeis e as caixas plásticas de cd.

Téo desviou a vista como quem busca resposta nas prateleiras, mas só encontrou os mesmos livros velhos de direito, os mesmos fichários e pastas e envelopes. Não podia ser tão terrível uma viagem daquelas, um favor ao primo, uma visita breve aos pais. Ficaria dois ou três dias em Buenos Aires e seguiria para onde quer que estivesse a pista mais conveniente. Era o tempo de recolher com Roberto as informações que faltassem, se é que haveria ainda algo mais a se saber, de trinta anos que aquele homem estava desaparecido.

Sabia que a tia havia recusado ajuda financeira da irmã, mãe de Téo — e por isso seus pais inventaram de mandá-lo à Argentina para terminar os estudos, que assim enviavam dinheiro para sustentá-lo e tinham desculpa para enviar um pouco mais, porque sim. Ninguém falava de Antonio naquela casa e para a tia era como se sequer houvesse existido. O primo Roberto tão criança parecia sempre mais preocupado com outros temas; Pablo, um pouco mais velho, ocupava-se de criar confusão, gritar com a mãe, brigar com os vizinhos.

Olhou Gustavo do outro lado da mesa, o meio sorriso que lhe era típico quando sabia que tinha razão mas estoico se segurava para não proclamá-lo em voz alta. Os olhos num castanho tão escuro que não deixavam ver as pupilas. Tinha os dedos entrelaçados na frente do corpo e mui vagaroso movia os polegares, em gesto distraído.

Téo pensava em tomar a viagem como respiro para se afastar dessa sensação de que estava atado em infalível desastre. Sentia que já estava demasiado acostumado à companhia de Gustavo, os sorrisos de Gustavo, as mãos de Gustavo

e tão pior a queda quanto mais alto

(a queda que sempre vem).

Então Gustavo se oferecia de motorista e companhia e _não conheço a Argentina_. Que a ele fosse só essa chance de aproveitar as semanas de folga que havia acumulado, fugir das esquisitices de São Paulo, sentir o vento na estrada. Téo deixou escapar um sorriso involuntário, e talvez aí Gustavo percebeu o desnecessário da agressividade. Pediu desculpas e se levantou, disse que precisava voltar ao trabalho. Inclinou-se por sobre a mesa para beijar a cabeça de Téo antes de se afastar.

Téo pensaria que _yo siempre lo supe_ era

(seria?)

a mais óbvia das reações e

por que pensava que escondia algo

(que precisava esconder algo)

se tinha afinal mais de quarenta anos. Se seu pai — esse tão presente ainda que em sua distração de homem tosco da roça — havia contrariado o conservadorismo da família de cidade pequena ao se casar com argentina metida a comunista e vá saber como ela tinha ido parar no interior paulista, naquela cidadezinha sem vento tão fora de qualquer caminho. Que desse mais crédito àqueles dois rebeldes que eram seus pais, que depois dos sessenta haviam abandonado aquele fim de mundo e se metido em Buenos Aires para abrir uma agência de viagens e recomeçar a vida.

Gustavo tinha a mão na porta. Téo quis dizer que _espera_ e _a coisa é um pouco mais complicada do que_. Mas Gustavo virou-se e sorriu:

— Não precisa se preocupar. Te ligo mais tarde.

## 14.

### Buenos Aires, BsAs

Téo jogou-se no banco do passageiro e fechou a porta num só movimento. Soltou o ar com força num suspiro exagerado e jogou aos pés do assento os livros que a mãe lhe havia emprestado. Custou a perceber que Gustavo o encarava — as mangas da camisa dobradas até antes do cotovelo e os óculos escuros por sobre a cabeça, tão inevitável turista brasileiro.

— Vamos — Téo resmungou — se a gente demorar mais tempo pra sair vamos acabar pegando trânsito.

Gustavo resignado conferiu o aparelho GPS afixado no painel do carro alugado e apertou na tela alguns botões. Uma voz feminina os cumprimentou em espanhol e indicou a direção que deviam tomar. Gustavo acomodou os óculos escuros no rosto e manobrou de volta à rua.

— Se ela falar alguma coisa esquisita você vai ter que me traduzir.

— _Izquierda, derecha_ — Téo retrucou — não é muito difícil.

O comentário soou mais agressivo do que teria sido a intenção, e Gustavo fez só erguer as sobrancelhas, como se registrando consigo a mudança de humor. Não que Téo houvesse acordado no melhor dos humores — os poucos dias que estiveram em Buenos Aires haviam sido exercício constante de medir palavras e testar reações, mais que o normal.

Téo não prestava atenção nele. Ouvia feito eco as palavras de sua mãe e queria pensar em algo que não Gustavo, mas voltava a pensar em Pablo e parecia que o cinto de segurança o sufocava. Não se lembrava a última viagem longa que havia feito de carro. Não dirigia, não tinha carta de motorista, não gostava de andar de carro.

Concentrasse a energia naquele tio desaparecido e esses vestígios improváveis que ele havia deixado pelo caminho.

Rumo a Córdoba.

— Ruta nueve — Gustavo murmurou, em seu sotaque aportuguesado, em tentativa de criar assunto quando já cruzavam Palermo e faltava ainda caminho para alcançar a borda da cidade. — Qual é aquela famosa?

— Aquela o quê?

— A rodovia.

— Quarenta. _Ruta cuarenta_.

— Você não conhece mesmo mais nada do país?

— Conheço um pouco de Córdoba.

— Só isso?

Téo buscou um dos livros aos pés do assento. Um livro de contos de Mario Benedetti. O outro era de Juan Jose Saer, _La pesquisa_. Pelo título não sabia se ficava irritado ou curioso — que passava na cabeça da mãe? De que lhe serviria a literatura quando a realidade era imensa e o país era imenso e bem fosse verdade seu tio podia estar ali mesmo em Buenos Aires, todo aquele tempo, anônimo.

— O dinheiro que eu tinha quando era adolescente eu gastava com ingresso de jogo, camisa de time, sorvete — Téo disse. — Não tinha para viajar.

— E depois de adulto? Você vem pelo menos uma vez por ano.

Téo pensou que continuava gastando boa parte do dinheiro das viagens com futebol e sorvete, mas

claro

não era de dinheiro que Gustavo estava falando. Seria talvez explicar que lhe eram suficientes a capital federal, o tempo com os primos e as conversas com a mãe, os hobbies inesperados do pai.

— Córdoba capital? — Gustavo perguntou.

Téo afirmou com um gesto de cabeça. O que conhecia da região era de um caso mal resolvido, uma investigação a pedido do primo Pablo. Uma história mal contada que o fez se meter onde não devia. Isso devia ter dito ao primo: vê o tipo de encrenca que me caiu em cima por sua causa e agora vem me acusar de.

Era história repetida?, investigar o que não devia, meter-se pela estrada rumo ao norte e buscar gente desaparecida.

(Como desaparecem os adultos?)

— Tem uma cidadezinha turística no caminho — Gustavo disse — antes da sua primeira parada. A gente podia ficar uma noite, dar uma volta. Dizem que por ali passam muitos discos voadores.

Téo riu e Gustavo, aliviado, fez o mesmo.

— Você acha que meu tio foi abduzido?

— Nunca se sabe!

O sorriso fez-se mais fraco, quase desaparecido. Fosse algo assim, absurdo. Mesmo a primeira parte da viagem já parecia impossível, interminável. Téo queria fechar os olhos e ficar em silêncio, abstrair o quanto possível o incômodo do percurso longo enclausurado naquela lata sobre rodas. Queria deixar para trás os últimos dias e se concentrar na investigação adiante. Não queria a obrigação da companhia e criar conversa e manter conversa. Mais ainda porque o assunto

(essas conversas pela metade

o jantar na casa de seus pais

o encontro com Pablo no dia anterior)

pairava pesado, ocupando o espaço vazio dentro do carro.

Discutir alienígenas não os salvaria de nove horas de estrada.

— A gente não precisa conversar sobre o que você não quer conversar — Gustavo disse, sério.

Quisesse apontar o elefante monstruoso em que se haviam transformado todos os não-ditos. Ou: que sejamos adultos, e que haja diálogo

ou não.

— De que você está falando? — Téo perguntou.

— Que foi que tua mãe disse?

— Ela não disse nada.

— Por isso você voltou mal-humorado e se jogou no carro como quem escapou da forca.

— Minha mãe fala muito.

Gustavo soltou um riso nervoso. Téo mantinha os olhos no livro, como se buscasse na capa cinzenta uma rota de fuga. Era possível que se esgotaria enfim a paciência infinita de Gustavo — ali no trânsito da manhã portenha, poucos dias depois que haviam chegado. Já Gustavo havia aprendido a se comunicar com Roberto e andar sozinho pela cidade sem que o atropelassem as palavras. Compreendia um pouco do espanhol apressado da gente local e sabia pedir _más despacio, por favor_ com seu sorriso enorme de brasileiro.

Talvez pensasse

(frustrado)

de que adiantava saber mais de um idioma se em nenhum deles Téo era capaz de se comunicar, fazer-se entender.

— Ela não disse nada — Téo repetiu. — Ela não disse nada, meu pai não disse nada. Que te parece? Que você acha que eles iam dizer?

— Você voltou irritado.

— O dia ontem foi ruim.

— Não estou falando do seu primo.

— Então qual o problema?

— Você disse que sua mãe só queria que você buscasse um livro. Se fosse só isso, você não teria voltado desse jeito.

— Se a situação fosse simples a gente nem precisaria ter esse tipo de conversa.

Era bem provável o mais sensato que Téo havia conseguido articular. Gustavo respirou fundo: talvez fosse melhor estar atento ao caminho e às instruções da voz eletrônica soando em língua estrangeira pelo aparelho GPS.

— Eu conversei com sua mãe, Téo.

— Como assim?

— Não, não — Gustavo quis rir, mas se conteve. — Quero dizer que a gente esteve com ela. Não entendo de que você tem medo. Sua mãe tem a cabeça aberta. Mais até do que a minha mãe. E seu pai também.

— Não é nada isso. Você não entendeu.

A viagem mal começada. Gustavo apertou o volante com força, a expressão de quem se esforça para engolir a frustração. Havia afinal se disposto a dirigir e se fazer assistente de detetive quando podia ter seguido ao Rio de Janeiro para visitar a própria família. Sabia desde o começo que não estavam ali para discutir a relação e muito provável que só por isso Téo havia topado que o acompanhasse.

Ligou o som do carro e perguntou sobre estações de rádio e pareceu que escapava dali uma nuvem pesada. Súbito mais animado Téo buscou na mochila o aparelho de mp3, gastou alguns minutos tentando descobrir como conectá-lo ao sistema do carro. Logo assumiria o posto de curador musical, como lhe convinha, esquecido do rumor do trânsito e da vibração ensurdecedora do motor.

## 2.

### São Paulo, SP

O silêncio da agência era o ruído dos carros na rua, que ali do oitavo andar parecia quase o murmúrio do mar, incansável. Téo meteu na mochila a câmera fotográfica e os acessórios todos: bateria, carregador, um emaranhado de cabos. Uma caderneta de anotações pequena, caneta, o aparelho de mp3 que sequer sabia usar. Sentiu-se um pouco atrasado — retrasado. Uns vinte anos se escondendo do mundo e agarrando-se a um ideal impossível de atemporalidade: a música em cds era-lhe ainda excesso de modernidade.

Sabia o que Gustavo ia dizer: o absurdo porque

(sim)

tinha mais de quarenta anos e os pais não sabiam que era gay, que Elisa era sócia e melhor amiga mas não era e jamais seria sua namorada ou amante secreta.

Ou: sabiam, como poderiam não saber?

E se sabiam

todo esse fingimento?

Gustavo tinha a mãe como confidente e um pai distante que encontrava em eventuais festas de família no interior do Rio de Janeiro; um meio-irmão mais velho vagamente homofóbico que ele tratava com um desprezo educado. Nenhum segredo, nenhum não-dito, nenhuma surpresa.

Era mesmo necessário?, explicar-se

chamar de _namorado_ e apresentar à família.

Téo havia saído de casa antes de completar quinze anos para terminar o colégio em Buenos Aires, na casa da tia, irmã gêmea de sua mãe. Não voltaria mais. Meteu-se depois na capital paulista e inventou de estudar direito porque parecia na época o melhor que podia fazer com sua _inteligência_, que era como os professores chamavam essa mistura nem sempre saudável de introspecção e capacidade analítica. Desistiria da carreira antes mesmo que terminasse a faculdade, e o diploma jamais lhe serviria para muito além de saber conversar com delegado de polícia e compreender as piadinhas jurídicas que Gustavo vez ou outra deixava escapar.

— Que horas é seu voo amanhã?

Elisa havia parado com o corpo no batente da porta. Téo fechou o zíper da mochila e jogou o corpo para trás.

— Três da tarde.

— Posso sair da agência meio dia e te buscar em casa, que te parece?

— Não precisa. Eu pego um táxi.

— Gustavo também vai?

Ela se aproximou para ocupar uma das cadeiras em frente à mesa. Pela janela fugia a tarde, naquela pressa de outubro pelos dias de verão futuro.

— Estou indo pra trabalhar.

— Pois bote um mapa de Buenos Aires na mão dele e vá atrás do seu tio enquanto ele conhece San Telmo.

Téo sentia-se à vontade com Elisa porque ela não exigia as explicações que ele imaginava que os outros queriam. Aceitava quando Téo mudava de assunto e permitia que crescesse entre eles o silêncio quando no fim do dia sentavam juntos no escritório e distraíam-se com a vista da janela e os ruídos da cidade. Compreendiam-se no silêncio, como convinha.

— Ele vai — Téo disse.

Gustavo saberia se fazer tonto, o quanto Téo lhe pedisse: dizer-se amigo e falar de mulher, como se bastasse. Era ridículo o fingimento, e tinha afinal mais de quarenta anos e já era tempo de deixar de lado as inseguranças e paixões platônicas que lhe haviam estragado a adolescência e o começo da vida adulta. Mas era também constatar o complicado que se tornavam os relacionamentos que duravam mais de duas ou três semanas. Como ia dizer que _esse é Gustavo, meu namorado_ se nem mesmo acreditava que.

Pensar no tio que

fugiu de casa?

e que desespero era esse?, uma vida tão errada que era preciso abandoná-la sem olhar para trás e desaparecer sem se despedir. Mais fácil seria concordar com o primo Pablo e _mi padre ese es un hijo de puta que me importa que le pasó_. Quanto desespero para fugir assim das responsabilidades, dos filhos pequenos, da esposa e da casa e

nunca mais.

— Não sei. Tenho a sensação de que—

— _I have a bad feeling about this_ — Elisa interrompeu, engrossando um pouco a voz.

— _That\'s no moon, that\'s a space station_.

Sorriso esboçado. Antes fosse Elisa insistindo para acompanhá-lo na investigação. Mas havia a agência e o trabalho _de verdade_ e era necessário pagar as contas e o aluguel do escritório em oitavo andar num edifício da Bela Vista. Elisa era quem cuidava das questões administrativas e lidava com os clientes — as funções mais difíceis de se delegar. Para as diligências sempre havia jornalista ou policial procurando bico.

— Você fica bem aqui sozinha?

Elisa riu.

— Alexandre me ajuda com a parte remota. E você sabe que Vitória está doida por um pouco de trabalho de campo.

A pergunta como mero protocolo: tudo que Téo já sabia. Era hábito desviar o tema e discutir trabalho quando a vida parecia demasiado complicada. Antes também investigar gente desconhecida e destruir matrimônios à distância com uma ou outra fotografia suspeita. Escrever relatórios, guardar a informação num envelope e adiante ao próximo caso. O glamour da profissão não existia nem mesmo no cinema

e para Téo e Elisa estava muito bom.

— Vocês estão juntos já tem alguns meses — Elisa disse. — Não é como se ele estivesse te apontando uma arma na cabeça.

— Eu sei. Eu gosto dele.

— Então?

Téo abriu a boca para responder, mas só fez transformar a expressão numa careta e soltar um _ah_ resmungado. Dizer que _não nasci para isso_ embora nem soubesse o que podia ser _isso_. Relacionamentos, intimidade, companhia silenciosa para o café da manhã e Gustavo no espelho do banheiro abotoando a camisa muito devagar feito guardasse nas mãos todo o tempo do mundo. Aquele _erre_ arranhado do sotaque carioca que Téo seguia escutando na cabeça por todos os lados, na cidade inteira.

Meter-se contra a corrente numa sociedade que exigia de quem não se adequasse uma declaração assinada da diferença: caminhar a todo tempo com bandeira hasteada. Tão cansativa essa constante afirmação reafirmação de identidade e bater o pé por exigir a monotonia de

(uma vida normal?)

Que tanto lhes importava se era gay ou canhoto ou.

— Podia ser suficiente — Téo disse.

— É suficiente.

Téo fez outra careta. Olhou sobre a mesa os papéis e envelopes espalhados e deu de juntá-los numa pilha. Encontrou os óculos de leitura e os guardou na caixa, enfiou na lateral da mochila.

— Obviamente não é. Tem que apresentar pra mãe.

Elisa fez que ia responder, mas Téo continuou:

— Não vai durar.

— Isso você diz quase todo dia.

Também Elisa não se entendia com relacionamentos. Era mais fácil desmontá-los com meia dúzia de fotografias. Isso sabiam fazer. Quisesse talvez concordar

(não vai durar)

porque tão mais conveniente o pessimismo do ofício e concluir que nenhum relacionamento é possível.

— Meio dia — Téo disse. — Vou preparar uns sanduíches e fazemos um piquenique no aeroporto.

## 27.

### El Bolsón, Rio Negro

A Patagônia era principalmente o vento. Fazia lembrar Córdoba e o vento norte, abafado, que _enlouquecia as mulheres_ e perturbava todo ser movente. Mas ali era só um vento frio que gastava a paisagem e as coisas e as pessoas e levantava uma poeira cinzenta de terra preta. Que volta absurda havia feito, de Córdoba a Tucumán, de Tucumán a Catamarca e de Catamarca ao sul

cada vez mais ao sul.

Pensou que terminaria em Ushuaia, _fin del mundo_, e nem mesmo ali encontraria o tio, desaparecido para além do espaço ou perdido pelo caminho numa curva ao lado errado. Cada passo adiante era também a chance de que Antonio houvesse ficado atrás, cada vez mais distante. Téo não se lembrava de uma Argentina tão enorme e impossível. Lembrava-se do percurso entre o colégio em Avellaneda e o porto, do centro de Buenos Aires ao estádio do Racing, a linha do trem por onde corriam para se esconder dos torcedores furiosos do time adversário.

A moça das calças coloridas e agasalho de lhamas organizava sua mesa com o fim da feira. Duas crianças brincavam por perto. Provocavam-se: a mais velha perturbava a menorzinha, e vez ou outra a artesã as interrompia com a expressão séria. Téo observava, distante, ainda a garrafa vazia de cerveja artesanal ao seu lado, um pote de plástico com um resto de framboesas.

Queria falar com ela — entender essa energia para montar e desmontar a mesa de artesanato todo domingo e quem sabe expor em outro canto no resto da semana, caminhar pela rua identificando turistas e sorrisos e se nada funcionasse quem sabe mudar de cidade, pedir carona na pista e descer onde parecesse conveniente. Fosse o que lhe restasse: colocar-se no lugar do tio e deixar que o destino se construísse nas incertezas.

— _Y vos? Te dejaron plantado?_

A artesã o observava alguns metros à sua frente: mochila nas costas, uma tábua grande debaixo do braço e os cavaletes que faziam as pernas da mesa pendurados no ombro. As crianças — um menino e uma menina — haviam corrido adiante. Téo se levantou, atrapalhado pegando no chão a garrafa e a embalagem de framboesas.

— _No, no_ — e conferiu as horas. — _Te ayudo?_

Ela pareceu considerar a oferta. Media-o e repensava o peso nas costas. Téo tentou um sorriso que se pretendia amigável, encolheu os ombros como se para mostrá-la que era inofensivo. A artesã se aproximou. Entregou-lhe as partes da mesa e tomou-lhe das mãos a garrafa vazia e as framboesas.

— _Justo_ — Téo disse. — _A ver si los niños quieren unas frambuesas?_

— _A ver._

Virou-se para onde estavam as crianças, gritou-lhes os nomes e ergueu o pote. A mais velha pareceu discutir a questão com o irmão pequeno, talvez para decidir se aceitavam a presença inesperada no caminho de volta para casa. Terminada a conferência seguiram até os adultos. A menina gesticulou para que o irmão pegasse a embalagem, e logo saíram correndo na mesma direção de antes.

Téo esperou que a artesã indicasse o caminho.

— _Soy Juan_ — disse, e sentiu daquela vez que lhe pesava o primeiro nome, feito estivesse falando de outra pessoa.

— _Violeta._

— _Vivís acá?_

— _Ahora, sí._

— _Hace poco tiempo?_

— _Un año, creo. Un año y unos meses._

Cruzaram a praça até alcançar a avenida. As crianças esperavam na esquina com o pote de framboesas vazio e logo tornaram a correr. Era quase sete da noite e o sol seguia firme no horizonte, flutuando por sobre o traçado irregular da cadeia montanhosa. Um vento gelado havia tomado as ruas, levantando poeira cinzenta e obrigando os mais friorentos a buscar os caminhos que ainda não haviam sido engolidos pelas sombras do fim do dia.

O povoado era todo plano, esticado no espaço de norte a sul como se houvesse sido trazido pelo vento. As calçadas de terra se confundiam com as calçadas de cimento e com o asfalto da pista. A artesã tinha a pele e os cabelos quase da mesma cor; uns olhos cinzentos que eram também da mesma cor de todo o resto do lugar.

— _Y vos?_ — ela perguntou. — _Vacaciones?_

— _No. Trabajo._

— _En serio? Qué hacés?_

Parecia demasiado complexo explicar o que fazia; dizer que era investigador particular e buscava um tio desaparecido trinta anos antes. Que não sabia mais o que estava fazendo, ali do outro lado do país, quando mais lógico teria sido encontrar o tio em outro canto, em outro clima, em outros ventos. Seguia por inércia: a _Ruta 40_ o chamava e ele se permitia deslizar por mais um instante.

Só mais um instante.

Fizeram atalho pelo calçamento de pedregulho de um estacionamento e dobraram à esquerda numa rua estreita. As crianças continuavam adiantadas, quase na esquina seguinte. Téo não conhecia o povoado o suficiente para saber a que lado iam. Sabia que não havia como se perder; bastava depois encontrar a avenida San Martín

(era sempre uma avenida San Martín)

e saberia tomar o rumo da hospedagem.

Explicaria: o pedido de um primo, o tio desaparecido décadas antes e o quanto não entendia de encontrar gente desaparecida e mais ainda desaparecida havia tanto tempo. Disse que naquele momento apenas seguia adiante, tateando de olhos fechados a poeira ancestral que parecia ter criado as cidades e povoados Patagônia adentro.

Violeta o ouviu atenta, com a vista nas crianças.

— _El viento trae todo tipo de cosas hacia acá, sabés_ — ela disse, com um sorriso enigmático, pensando talvez em seus próprios motivos para estar ali, na divisa entre as províncias de Rio Negro e Chubut, aos pés dos Andes.

Entraram em uma ruazinha de terra, ladeada de casas miúdas com pinta de chalé. As crianças pararam junto a um portão baixo de madeira, e com um salto desapareceram atrás dele. Via-se uma área grande de jardim, algumas barracas; ao fundo uma casa grande com as paredes pintadas de amarelo. Próximo ao portão havia uma placa de madeira com as letras em branco: "camping".

A rua canalizava uma brisa fria, e as árvores impediam que os alcançasse o sol. Seguiram até o portão e Violeta estendeu as mãos para tomar de Téo as partes de sua mesa.

— _Te quedás más tiempo en el pueblo?_ — ela perguntou.

Téo hesitou, deu de ombros. Não sabia mais a que lado seguir. Ou ia adiante pela RN40, porque

por que não? A investigação toda apontava naquela direção: ao sul, sempre ao sul

cada vez mais ao sul.

Que diferença fazia?

O país era todo sul.

As mensagens com o primo ficavam cada vez mais esparsas. Na última delas, alguns dias antes, Roberto não fez perguntas. Escreveu, apenas

Gustavo está acá

esperando talvez uma explicação

porque Gustavo não soubera se explicar

sequer havia tentado.

Téo não escreveu de volta.

— _Te gusta caminar?_ — Violeta perguntou.

Ele afirmou com a cabeça.

— _Mañana vamos al cerro Piltriquitrón_ — ela disse — _yo y unos amigos, sin los niños. Vení con nosotros._

## 3.

### GRU -- EZE

Claro que era ridículo: o tapinha na mão meio disfarçado, meio demorado, quase sem querer. Feito fossem adolescentes que não podiam revelar uma paixão proibida. Era ridículo porque sim, era homem de quarenta e quatro anos e Gustavo sabia, agia daquela forma de propósito, em esforço para se adequar ao que quer que fosse o ideal de relacionamento de Téo. Aquele relacionamento inexistente que até a comissária de bordo adivinhava, como se não houvesse outra possibilidade na intimidade que se mostrava nas entrelinhas dos movimentos.

— Odeio avião — Gustavo disse.

— Se pudesse você ficava pra sempre no mesmo lugar.

Gustavo hesitou em responder, mas acabou concordando com a cabeça.

— Você nunca viajou pra fora? — Téo perguntou.

— Do país? Ah, sim, uma vez.

— Com Fabiano.

A resposta foi um sorriso; aquele sorriso. Téo desviou o olhar para prestar atenção na gente que circulava pelo corredor procurando o assento marcado na passagem, pedindo licença e se espremendo por entre os outros. Téo que tanto havia se mudado — de cidade, de país, de vida — preferia também o endereço fixo e a rotina num só espaço: a avenida Paulista e o centro de São Paulo. Talvez porque tempo demais havia sido estrangeiro e estava cansado de não pertencer. Ou porque jamais seria capaz de pertencer: brasileiro filho de mãe argentina torcedor do Racing Club de Avellaneda _que pensa que Maradona é melhor do que Pelé_ — como diria um amigo jornalista, indignado.

Tinham isso em comum, ele e Gustavo: querer fincar os pés num só lugar e não sair mais. Ambos longe da cidade natal inventando de transformar São Paulo em lar doce lar.

— Senhoras e senhores, aqui fala o comandante.

Os comissários ainda se moviam de ponta a outra do corredor com os últimos preparativos e as instruções de segurança. A conversa tinha acabado ali, minutos antes, com o nome do ex-namorado de Gustavo atirado no vazio. Não que fosse tema proibido. Téo evitava o assunto porque sabia que Gustavo não se importava de falar sobre ele. Gustavo não o olhava, mas era inevitável o contato físico porque ambos se apoiassem no braço entre as poltronas e era preciso preencher o espaço com palavras ou

bem-vindos a bordo do voo 4589

o tempo em Buenos Aires

temperatura atual de 23 graus

tempo estimado de voo

senhoras e senhores

tirar do bolso a embalagem de chiclete comprada no aeroporto, oferecer a Gustavo _porque a pressão_ etc. Ele aceitou com um sorriso, outro sorriso.

Gustavo, sempre

decolagem autorizada

sorrisos.

Gustavo fechou os olhos enquanto o avião ganhava velocidade, encolhido como convinha ao espaço do assento. Era mais alto que Téo — passava fácil de metro e noventa. Também um tanto mais forte: bicho que cresceu na praia e acostumou-se a passar o dia sem camisa sob o sol. Téo o ouviu respirar fundo e quis lhe tomar a mão, mas.

Você sabe que.

Você sabe que meus pais.

Meus pais.

Porque minha mãe.

E você não está um pouco grandinho para esse tipo de discurso? As palavras davam voltas e o avião se sacudia com o vento ao ganhar altitude.

— Qual vai ser a história, Téo?

Gustavo virou a cabeça de lado, amassando os cachos desordenados que lhe escapavam atrás da orelha. Assim sentados os olhos se encontravam quase na mesma altura. Téo quis rir, mas sentiu foi os pelos do braço se arrepiarem, um frio na barriga

(a pressão e esse incômodo nos ouvidos).

— Roberto é o único que sabe.

— Seu primo mais novo.

— Sim.

— E você vai contar?

Quis dizer que sim, claro, ou

que não interessava a ninguém e não fazia diferença.

Abriu a boca, deteve-se. Havia se arrastado até ali com respostas evasivas

aos pais, ao primo mais velho

a Gustavo.

Porque quarenta e quatro anos e voltavam às vezes feito eco maldito as brincadeiras idiotas de moleque machinho latino-americano. O constante estado de alerta porque isso não pode dizer e isso não pode fazer e as regras todas não-escritas desvendadas _a las patadas_. Pensava nos amigos de colégio e nos amigos de Pablo com quem vez ou outra havia ido ao estádio depois de adulto quando voltava para visitar. Pensou que sim: estava velho demais para aquele tipo de insegurança.

Ninguém se importava.

(Não fazia diferença.)

Pensou em Fabiano e a história trágica que Gustavo obstinado carregava nas costas como se não doesse

como se não pesasse o passado

como se não importassem essas brincadeiras de moleque

tão inofensivas.

— Tenho certeza que tua mãe é muito mais esperta do que você pensa — Gustavo disse.

Claro que era. Quem não saberia? Seu pai se fazia tonto e toda vez perguntava quando ele ia tomar vergonha na cara e pedir Elisa em casamento. _Vocês se dão tão bem_.

— Eu sei o nome de sua mãe?

— Julieta.

— Julieta — Gustavo repetiu, imitando a pronúncia em espanhol. — E teu pai?

Não seria a primeira vez que mencionava a Gustavo o nome de seus pais, mas era súbito tão real a proximidade dos universos sentia-se cometendo uma transgressão perigosa.

— Joaquim.

— Julieta, Joaquim, Juan Teodoro.

Téo moveu-se no assento, afrouxou o cinto de segurança. Bastava cruzar a fronteira para que mudasse de nome. A mãe sempre lhe havia chamado Juan e o pai Teodoro. Eram os nomes dos avós, o argentino e o brasileiro.

— Você acha mesmo que Roberto não contou pro teu outro primo, pra tua tia?

— Eu conheço meu primo.

— E eles conhecem você.

O avião se estabilizou depois de uma curva, e pela janela via-se a represa Guarapiranga, enorme. A cidade de São Paulo feia e cinzenta um emaranhado de blocos de concreto. Gustavo lhe havia contado o quanto antes queria ter seguido carreira no teatro em vez de estudar direito. Mui fácil se encaixaria no papel que lhe fosse atribuído. Era o que fazia no trabalho, embora não se preocupasse em esconder dos colegas coisa nenhuma: entrava sem esforço nas conversas quando davam de discutir mulher e carro e essas coisas que — ele reclamava — homem hétero tanto gosta.

— A vida é tua, cara — Gustavo disse. — Mas eu preciso saber a história que você quer contar se for para participar do mesmo jogo.

Procurou no bolso da poltrona da frente algo com o que se distrair, desistiu e fechou os olhos, e Téo se distraiu observando-o. Gostava de observá-lo, mais do que seria capaz de admitir. Gostava de ver o cuidado com que se vestia ou a tranquilidade com que descalçava os sapatos; a forma como pousava a carteira alinhada sobre a mesa ao lado dos óculos escuros e das chaves do carro; a linha dos cabelos fazendo curva atrás da orelha.

Não conversaram mais para além de um comentário sobre o lanche e Téo disfarçou o desconforto quando Gustavo sorriu demais para um dos comissários. Gustavo, os sorrisos. Lógico que não era ciúmes. Ciúmes exigia alguma vaga noção de posse e

(convenhamos)

isso Téo não tinha, não podia ter

(lógico que não era ciúmes).

Esperaram em silêncio enquanto todos se levantavam e tiravam as malas dos compartimentos, metiam-se pelo corredor entre as poltronas e aguardavam que se abrisse a porta. Afinal não havia história senão pelas lacunas: _esse é Gustavo_. Que estava esperando? Gustavo não era de se fazer tímido ou calado. Não houvesse um papel a ocupar, Gustavo seria o que melhor sabia ser: ele mesmo.

Então as luzes do aeroporto, a confusão do _duty free_ e o controle da aduana, a aglomeração em volta da esteira de bagagem e Téo não se lembrava de em outros momentos se sentir tão nervoso ao atravessar o corredor do desembarque. Reconheceu entre os rostos estranhos a expressão sorridente de sua mãe ao lado do primo Roberto. O primo ergueu o braço e se adiantou, os olhos brilhando por trás do reflexo esverdeado nas lentes dos óculos.

Abraçaram-se.

— _Hiciste buen viaje?_

Roberto lhe tomou a mochila maior e voltou a atenção ao homem atrás dele: o carioca um tanto mais alto, um tanto mais sorridente, a pele um tom mais escuro.

— _Hola! Roberto, el primo_ — e lhe estendeu a mão.

— Gustavo

(o namorado).

Sorriram-se. Julieta abraçou e beijou Gustavo como quem cumprimenta amigo antigo, disse em português que bem-vindo e como foi a viagem e que bom que o voo não atrasou assim não pegamos tanto trânsito.

— _Trabajás con Juan?_ — Roberto perguntou.

Gustavo buscaria em Téo a resposta certa, a história que não haviam inventado. Podia dizer que era o assistente ou o motorista, mas duvidou talvez de sua capacidade de falar sem dar risada da invenção. Fez-se distraído enquanto conferia o zíper no bolso da mochila e afinal tinha a barreira do idioma como desculpa.

— _Es un amigo_ — Téo respondeu, mais para a mãe do que para o primo, e ficasse assim estabelecido o que havia para se estabelecer. — _Quería conocer a Buenos Aires, qué sé yo, cada loco con su tema_.

Roberto repetiu _bienvenido_ e _está buenísima la ciudad si no vivís acá_. Já era Gustavo centro das atenções: Roberto tentava lhe fazer perguntas no melhor de seu portunhol para saber de onde se conheciam e que você faz e já sabe o que quer ver na cidade? Seguiram no rumo do estacionamento e a mãe tomou de Téo o braço, colou o corpo ao dele.

— Bom te ver, _mi hijo_.

Assim nessa mistura de idiomas que se havia tornado comum nas conversas entre os dois.

— Assim que a gente terminar de chegar eu vou ver vocês — Téo disse. — _Vine para trabajar_.

— _Dale, dale_ — Julieta retrucou. — _Para vos es todo trabajo todo el tiempo, chico. Llegá, tomá un mate. Tu tío el boludo este no va a estar más desaparecido que ya está._

A última parte disse em voz baixa, e conferiu que Roberto continuava tentando conversa com o mui brasileiro Gustavo.

A mãe ainda falava quando saíram ao estacionamento: contava sobre o trabalho e a política e os vizinhos. As conversas sobre política e literatura eram também estar de volta à terra de sua mãe, ao mundo de sua mãe. A familiaridade que lhe escapava porque o coração em descompasso feito fosse criminoso prestes a ser descoberto pela polícia e pelo amor de deus, Téo, você já apanhou de marido adúltero ofendido, fugiu de bandido armado, sobreviveu ameaça de morte — esses riscos ocupacionais que ninguém espera fora da ficção. Visitava Buenos Aires quase todo ano e nunca essa sensação de garganta amarrada

esse desespero sem propósito.

Devesse fingir que

nada

estava tudo bem. A pressão da convivência e as obrigações de família e a presença de Gustavo fazendo onda em castelinho de areia mal construído. Explicar-se, justificar-se. Como fazia essa gente? Antes meter-se num cinema e

claro, os cinemas de Buenos Aires

as livrarias

as lojas de livros usados onde se escondia um livreiro barbudo e rabugento resmungando sobre a falta de cultura dos argentinos. Perder-se na avenida Corrientes que toda sua adolescência havia sido como o centro do mundo

ou o segundo centro do mundo

(depois do estádio _del Racing Club_).

A cidade era toda nostalgia e sempre que voltava Téo sentia que perdia mais um pedaço da memória; que o passado se emaranhava no presente e o presente se escondia atrás do passado e era de repente um adolescente de quarenta e quatro anos procurando na avenida uma loja de discos que não existia mais.

## 15.

### Capilla del Monte, Córdoba

Adormeceu passado o lago antes de Cosquín, no início do Valle de Punilla, menos de uma hora depois da cidade de Córdoba. Haviam almoçado na capital da província e seguia arrastado aquele segundo dia de viagem, depois de pernoite num povoado minúsculo brotado na planície aos pés da _Ruta 9_.

Gustavo o chacoalhou de leve e Téo despertou num susto. O zunido da estrada havia sido substituído por um silêncio pesado. O carro estava estacionado em uma rua estreita, antes de uma intersecção, em meio a um cenário pacato de cidadezinha entre montanhas, construções baixas e comércio fechado. O sol flutuava a oeste, atrás deles, fazendo que se esticasse adiante a sombra do carro. Na esquina em frente havia uma placa turística mostrando o que parecia ser o mapa do povoado. Téo se ajeitou no banco, apertou os olhos.

— Chegamos — Gustavo disse.

— Cruz del Eje?

— Capilla del Monte — e apontou a placa.

As árvores miúdas nas calçadas tinham as folhas novas num verde vivo de primavera. Passado o cruzamento a rua se transformava em atração turística e parte dela seguia sob uma estrutura coberta. O nome do lugar se repetia na fachada da cobertura para o caso

talvez

de alguém esquecer onde estava.

— Eu tinha dito que a gente podia parar aqui por um dia — Gustavo continuou. Desligou o GPS e virou-se para buscar a mochila atrás do banco. — Não custa descansar um pouco da estrada antes de começar a investigação.

Téo preferia que seguissem direto, e alcançariam o norte da província no fim da tarde. Em Cruz del Eje estaria o primeiro pedaço de investigação que lhe cabia: um endereço, uma amizade antiga. Não tanto que tivesse pressa. Depois de trinta anos, um dia a mais ou a menos não devia fazer diferença. Gustavo era quem tinha contados os dias de folga e precisava medir o tempo para estar de volta a Buenos Aires e não perder o voo. Era mais porque a distração criava espaços e os espaços gritavam por preenchimento.

— Que horas são?

— Três e algo. A gente podia tomar um sorvete.

Téo soltou o cinto de segurança e abriu a porta, saiu do carro. À parte a apreensão da pausa desnecessária, era um alívio pôr os pés no cimento mui imóvel da calçada. Deixou que o tomasse o ar da tarde, os cheiros desconhecidos. Da intersecção se viam as ruas todas vazias, silenciosas. Gustavo gesticulou na direção da rua coberta adiante e Téo percebeu no gesto a hesitação

tão acostumado a observar o movimento preciso das mãos de Gustavo

um tremor nos dedos ou o pulso que virava de leve em contração involuntária.

— Aqui estão os alienígenas, então.

— Cerro Uritorco — Gustavo arriscou, atrapalhado na pronúncia dos erres.

— _Cerro_ — Téo corrigiu, com um sorriso sonolento. — Eles moram na montanha?

— Acho que a montanha atrai os discos voadores.

— Humano mesmo não estou vendo nenhum.

Pararam depois de atravessar o cruzamento. Um cachorro dormia sob a sombra de um banco. Os negócios ao longo da parte coberta estavam fechados. Plena tarde estariam todos recolhidos para a _siesta_. Se soubessem onde procurar encontrariam talvez uma hospedagem aberta, o dono provavelmente dormido na recepção.

Seguiram pela rua, no passo lento dos turistas. A cobertura tomava pouco menos de um quarteirão e também os negócios rareavam depois dela. Téo se conteve para não decretar que seguissem viagem e deixassem de lado os desvios de percurso desnecessários, os sorvetes desnecessários

as conversas desnecessárias.

— A gente pode olhar o mapa — Gustavo disse, e apontou outra placa, idêntica à que havia próxima ao lugar em que estacionaram. — Tem alguns rios, umas trilhas. Preciso caminhar um pouco, estou cansado de dirigir.

Parou ao lado do mapa e distraído seguiu com o dedo as informações escritas na lateral. Téo viu que mais uma vez o movimento se fazia estável e seguro, como se inabalável. Chegou-lhes um murmúrio de crianças gritando, um motor de carro distante.

A viagem até ali havia sido essa presença imperturbável com as mãos firmes no volante e os gestos medidos ao arrumar os óculos escuros, coçar a barba, descansar o braço sobre a perna nos trechos de planície em linha reta que faziam a rodovia parecer infinita. A imensidão da pampa havia servido para aumentar o desconforto do carro fechado e a sensação de que não tinha para onde correr. Era tudo horizonte, por todos os lados.

Tampouco sabia de que queria fugir.

Antonio sabia de que estava fugindo?

Não havia vento, e o sol os atingia pelas costas, mas a temperatura estava agradável. Adiante por trás das construções adivinhava-se sob as nuvens o contorno da serra. Téo gostava de ver o verde brilhante das folhas novas que insistentes brotavam de troncos podados durante o inverno.

Gustavo se aproximou e passou o braço por sobre seus ombros.

— Topa uma trilha amanhã de manhã? — ele perguntou. — Depois do almoço a gente segue viagem.

A cabeça dizia _não_ mas pareceu mais fácil aceitar e fazer que sim, desacostumado com que alguém lhe tomasse as rédeas do dia e decidisse por ele o caminho a tomar. Controlou a vontade de se afastar e esperou que Gustavo sozinho criasse espaço entre eles, guardasse as mãos nos bolsos da calça. Que o tempo juntos fosse sempre preenchido com esses gestos incertos e o que poderia ter sido

(a rua vazia)

como se todo instante significasse um recomeço

renovadas as incertezas e inseguranças de princípio de relacionamento.

À noite Gustavo fez o brasileiro curioso e encontrou o centro de informações ao turista. No dia seguinte sugeriu um passeio para ocupar as primeiras horas da manhã. Poderiam voltar à estrada depois do almoço e em uma hora estariam em Cruz del Eje. Téo aceitou conformado a tranquilidade da serra e o esforço de Gustavo para disfarçar esses momentos em que os gestos deixavam transparecer um lapso de hesitação. Que fizesse questão daquele desvio na montanha não era tanto um problema. Era fácil estar com ele quando havia o que fazer e o assunto não corria de volta ao tema do relacionamento, ao que eram e aonde iam e o que queriam e o tempo, as horas, as semanas

os meses.

O relacionamento mais longo de Gustavo havia durado quinze anos e só terminou porque

uma tragédia.

Téo

quanto tempo tinha junto a Gustavo?, seis ou sete meses? Como podia gostar tanto da companhia de alguém e ao mesmo tempo

esse ímpeto de escapar

desaparecer.

Devia estar pensando na investigação, organizando notas e hipóteses, planejando percursos. Pouco adiantava pensar no tio como figura abstrata com razões obscuras ou constatar o incômodo por sentir que o compreendia

(talvez)

que a fuga era absurda mas

tão absurdamente coerente.

Saíram da hospedagem antes das sete. O movimento de turistas em outubro era quase inexistente, e àquela hora da manhã também não havia criatura que em férias decidisse acordar e seguir pela trilha contra o vento frio. O sol surgiu por trás da serra ameaçando dia quente ainda que na sombra o ar continuasse gelado.

O rio corria preguiçoso por entre as pedras. O esverdeado da água se iluminou com a luz do sol e Gustavo tirou do bolso a câmera digital. Téo parou atrás dele, buscou os óculos escuros na mochila. Gustavo apontou a câmera em sua direção.

— Faça cara de quem não está me odiando.

Téo ajustou o boné na cabeça.

— Não tinha percebido o quão argentino você é até te ver metido entre os argentinos — Gustavo disse.

— Que isso quer dizer?

— Você se encaixa no cenário.

— Aqui?

— Entre os argentinos.

— Eu tenho cara de argentino?

— Mais do que eu imaginava.

— Você continua com a mesma cara de brasileiro.

Téo não se sentia nem brasileiro nem argentino, ou ao menos não o suficiente para sentir que pertencia em definitivo a um dos lados da fronteira. Mesmo na adolescência era sempre _el gringo_, com ou sem o sotaque que não tinha. Como se jamais pudesse pertencer tanto quanto em meio à torcida no estádio de futebol, fazendo desaparecer o próprio grito junto ao grito da multidão.

O tio havia nascido em Catamarca, crescido em duas ou três cidades diferentes, seguido a Tucumán para trabalhar e

Buenos Aires, família, filhos.

Que se sentisse também estrangeiro, um pouco errado

um pouco gringo.

Isso Téo podia compreender. O que não entendia era o impulso para abandonar tudo, meter as roupas numa mala e correr atrás de um horizonte impossível. Se Antonio houvesse de fato seguido ao norte, certo teria atravessado a pampa, sentido pesar no peito a imensidão da paisagem vazia. Qual fosse a determinação e o tamanho do desespero ou

medo?

Quanto tempo Téo ainda aceitaria a convivência com Gustavo, a boa companhia, esses almoços de vinte minutos em dia de semana. Mais seis meses

seis semanas

seis dias.

Gustavo perdeu o equilíbrio com uma passada entre pedras e Téo avançou ligeiro para lhe tomar o braço, deixou-se trazer mais perto para um beijo. Gustavo lhe sorriu. Era todo o estereótipo do modelo brasileiro de exportação: a pele um tom de bronze, os cabelos cacheados que ele mantinha curtos, a barba que ele aparava com diligência antes de sair para trabalhar e que ali longe do escritório havia deixado por fazer; esses olhos castanhos quase pretos e o sorriso tão fácil. Alguns meses longe do Rio de Janeiro e o bronzeado certo lhe falhava, acostumado que estava ao terno e gravata no dia a dia de advogado, mas

adaptava-se.

Estava em casa.

Surgiu adiante um poço mais fundo formado na sequência de uma queda d\'água pequena. Uma clareira na lateral fazia do lugar um bom ponto para descansar. Gustavo descalçou o tênis, livrou-se da mochila, tirou a camiseta. Aproximou-se do rio e fez careta quando meteu os pés na água.

— _No estás en la playa, brasilero_ — Téo riu.

— Água gelada da porra.

— Me dê a câmera que tiro uma foto se você conseguir entrar. Você pode dizer pra tua mãe que estava cinco graus.

— Próxima vez que eu tiver folga eu vou é pra Bahia.

Sentou-se para esticar as pernas no sol. A cicatriz enorme que tinha na barriga ficava mais marcada com o tanto que havia perdido a cor nos meses vivendo em São Paulo. Buscou a câmera no bolso da bermuda para fazer algumas fotos e Téo se acomodou ao lado dele. Procurou na mochila o livro e os óculos.

(Calmaria.)

— Que você acha? — Gustavo meio de costas, observando a vegetação.

Apoiou as costas na pedra junto a Téo e virou-se com o braço encostado no dele. Téo se fez distraído, trocou os óculos escuros pelos de leitura e abriu o livro. Não tinha vontade de conversar. Sentia cada vez mais a presença de Gustavo como um desastre anunciado

inevitável.

— De quê? — os olhos ainda no livro.

— Seu tio.

— A última vez que fui investigar gente desaparecida encontrei foi um monte de ossos — disse, e espiou Gustavo por cima dos óculos, deixou escapar uma tentativa mal calculada de sorriso.

Gustavo lhe devolveu o mesmo sorriso torto, em espelho involuntário, e Téo voltou a atenção ao livro. A edição em papel jornal estava maltratada na lombada, como se tivesse tomado chuva ou sofrido outro tipo de descuido aquático, e Téo deixou-se distrair com a linha da mancha nas bordas do papel, esqueceu as palavras no meio da página. Sabia que sua mãe tinha com livros esse tipo de relação desleixada típica de quem cresceu entre eles e aprendeu a valorizar muito mais as palavras do que o objeto, para assombro de seu pai, homem simples do interior a quem havia sido ensinada a importância suprema desses receptáculos mágicos de conhecimento.

O casamento de seus pais havia perdurado contra toda lógica e expectativa, contra a distância cultural e as diferenças sociais — universos opostos mui confortavelmente acomodados juntos ao outro. Sua mãe certo teria algo a dizer sobre esse esboço de relacionamento que ele teimava em arrastar com Gustavo, mas.

— Você ainda está pensando na conversa com seu primo? — Gustavo perguntou.

Como se pudesse ler seus pensamentos e detectar a leitura desatenta. Téo custou a encontrar resposta, sacudiu a cabeça, deu de ombros.

— A gente não vai falar sobre isso.

— Porque talvez ele só precise um pouco de—

— Gustavo, por favor.

— Só queria dizer que—

— Não — Téo interrompeu. — Não vamos falar sobre Pablo.

Gustavo encolheu-se com os braços cruzados. Téo leu no livro a primeira linha, perdeu-se, voltou ao começo. _Allá, en cambio, en diciembre, la noche llega rápido_. As palavras pareciam esvaziadas de sentido. Não ia conseguir ler merda nenhuma. Quis dizer que

desculpa, mas

(as palavras).

Gustavo se distraiu com a vista no movimento do rio. Deve ter notado a hesitação, porque gesticulou em direção ao poço, as sobrancelhas erguidas e a expressão curiosa, como se para perguntar se Téo pretendia entrar na água.

— Eu posso ter cara de argentino mas nasci no interior de São Paulo — Téo disse. — Esse negócio de água fria não é comigo.

— E se eu entrar?

— Não é uma competição.

— Mas eu sou do Rio.

— Que isso tem a ver?

— O Rio é mais quente que o interior de São Paulo.

— E daí?

— Se eu conseguir entrar, você não tem desculpa.

Téo olhou Gustavo, tão carioca: descalço e sem camisa e os óculos escuros que ele quase nunca tirava quando ao ar livre.

— Já te falei o nome da cidade em que nasci? — Téo perguntou.

— Não.

Téo fez escapar um grunhido vitorioso. Retomou o livro e arrumou os óculos, por dizer que estava encerrada a conversa.

— Isso significa que você não vai dizer o nome da cidade?

— Significa que não vou entrar na água! — Téo retrucou, e foi incapaz de conter o riso, contrariado.

Tornou a abrir o livro e fingiu se concentrar nas palavras que flutuavam fugidias na primeira página da novela. Gustavo acomodou o corpo contra a pedra, conformou-se com o silêncio. Que se contentasse com a companhia e aproveitasse a pausa até a hora de voltar à estrada.

Talvez fosse suficiente.

## ...

_O livro esteve disponível de forma aberta aos interessados durante todo o processo de escrita. Agora eu peço a sua ajuda para publicá-lo: conheça [aqui o projeto de financiamento coletivo para a edição de NORTE](https://www.catarse.me/norte_olimaia)._