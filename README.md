# Norte

Novel. PT_br.

#### Sinopse

> No espaço entre Buenos Aires, os desertos de Catamarca e a Patagônia aos pés da cordilheira andina, Téo Miranda está deslocado de seu território habitual. Acostumado a investigar adultérios na São Paulo urbana, o detetive particular descobre-se em paragens remotas, contra o vento e a poeira, na busca por um tio desaparecido há mais de trinta anos.
> 
> Marcada por curvas imprevistas e a aridez da _Ruta Nacional 40_, a investigação se torna ainda mais difícil pela companhia insistente de Gustavo, com quem Téo mantém um relacionamento incerto. O detetive logo se dá conta de que para encontrar o tio precisa compreender seus motivos, mas a tarefa o obrigará a enfrentar suas próprias inseguranças — ou correr o risco de se perder nesse eterno meio de caminho no rumo do _fin del mundo_. 

"Qué sé yo, es desierto y hay mucho viento." -- Téo Miranda.

#### Status

12/jun/2023: lançada a [campanha de financiamento coletivo para a edição do livro](https://www.catarse.me/norte_olimaia).
19/mai/2022: terminada primeira revisão do livro; ele ainda vai passar por um monte de revisões e mudanças, como convém.
17/mai/2022: terminada primeira versão do livro.

#### Projeto

Ao longo da escrita fui atualizando este repositório ([codeberg.org/oli/norte](https://codeberg.org/oli/norte)) conforme modificava o texto para quem estivesse interessado em ler e acompanhar. Tirei o texto completo do ar para o lançamento da campanha de financiamento coletivo, mas ainda é possível conhecer os primeiros capítulos, já na versão definitiva.

#### SRC

Uso um programa open source chamado [novelWriter](https://novelwriter.io) que usa formatação em markdown e permite organizar notas, visualizar contagem de palavras e criar referências entre notas e capítulos com facilidade de forma simples. O programa exporta em markdown comum com dois cliques, que é como jogo o texto aqui no repositório. Não compartilho todos os arquivos do projeto porque escritor tem vergonha das próprias anotações. 

Já usei:

- libreoffice (pois é)
- wordgrinder (bom editor de texto de linha de comando)
- ghostwriter (editor de texto em formato markdown)

#### Arquivos adicionais no repositório

A pasta "/insp\_" guarda imagens relacionadas ao projeto.

O arquivo "notas.md" é autoexplicativo, mas não tem muita coisa lá. Foi meu ponto de partida para a escrita do romance. O arquivo de notas verdadeiro está oculto porque é uma bagunça.

Em "/src" há também arquivos gerados por scripts que calculam e informam meu progresso via contagem de palavras.

#### Autor

Oli Maia, artista e escritor desterrado.

Mais informações sobre meu trabalho no meu site: [olimaia.net](https://olimaia.net).
